import React , {useState} from "react";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";

function Counter() {
  return (
    <div className="counter-main-div">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 col-md-3">
            <VisibilitySensor partialVisibility offset={{bottom: 100}}>
              {( {isVisible} ) => (
                <h1 className="num">
                  {isVisible ? <CountUp start={0} end={5} duration={2} /> : '0'}
                </h1>
              )}
            </VisibilitySensor>
            {/* <h1 className="num">
              <CountUp start={0} end={7} duration={2} />{" "}
            </h1> */}
            <h3>Countries</h3>
          </div>
          <div className="col-12 col-md-3">
            <VisibilitySensor partialVisibility offset={{bottom: 100}}>
              {( {isVisible} ) => (
                <h1 className="num">
                  {isVisible ? <CountUp start={0} end={20} duration={2} suffix={" +"}/> : '0'}
                </h1>
              )}
            </VisibilitySensor>
            <h3>Clients spread across the globe</h3>
          </div>
          <div className="col-12 col-md-3">
          <VisibilitySensor partialVisibility offset={{bottom: 100}}>
              {( {isVisible} ) => (
                <h1 className="num">
                  {isVisible ? <CountUp start={0} end={250} duration={2} suffix={" k"}/> : '0'}
                </h1>
              )}
            </VisibilitySensor>
            <h3>Hours of projects delivered</h3>
          </div>
          <div className="col-12 col-md-3">
          <VisibilitySensor partialVisibility offset={{bottom: 100}}>
              {( {isVisible} ) => (
                <h1 className="num">
                  {isVisible ? <CountUp start={0} end={50} duration={2} suffix={" +"}/> : '0'}
                </h1>
              )}
            </VisibilitySensor>
            <h3>Team members</h3>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Counter;

import React from "react";
import Android from "./images/Android-Logo.svg";
import Ios from "./images/IOS-Logo.svg";
import Java from "./images/Java.svg";
import NODE from "./images/Node.js-Logo.wine.svg";
import Php from "./images/PHP-Logo.svg";
import ReactLogo from "./images/React.svg";
import Wordpress from "./images/WordPress-Logo.svg";
import Net from "./images/Net-logo.svg";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "./css/partner.css";
// import { FaAws, FaGoogle } from "react-icons/fa";

const options = {
  responsive: {
    0: {
      items: 2,
    },
    400: {
      items: 2,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 4,
    },
  },
};

const Partner = () => {
  return (
    <div className="container our-partners text-center">
      <OwlCarousel
        className="owl-theme"
        {...options}
        dots={false}
        autoplay={true}
        items={4}
        loop={true}
        nav={false}
      >
        <div class="item">
          <img src={Android} alt="" className="filters" />
        </div>
        <div class="item">
          <img src={Ios} alt="" className="filters filter-ios" />
        </div>
        <div class="item">
          <img src={Wordpress} alt="" className="filters" />
        </div>
        <div class="item">
          <img src={Java} alt="" className="filters filter-java" />
        </div>
        <div class="item">
          <img src={NODE} alt="" className="filters" />
        </div>
        <div class="item">
          <img src={ReactLogo} alt="" className="filters" />
        </div>
        <div class="item">
          <img src={Php} alt="" className="filters filter-php" />
        </div>
        <div class="item">
          <img src={Net} alt="" className="filters filter-net" />
        </div>
        <div class="item">
          <img src={ReactLogo} alt="" className="filters" />
        </div>
        <div class="item">
          <img src={Php} alt="" className="filters filter-firebase php" />
        </div>

        {/* <div class="item">
          <img src={Android} alt="" className="filters filter-google android" />
        </div>
        <div class="item">
          <img src={Ios} alt="" className="filters ios" />
        </div>
        <div class="item">
          <img src={NODE} alt="" className="filters" />
        </div>
        <div class="item">
          <img src={ReactLogo} alt="" className="filters" />
        </div> */}
      </OwlCarousel>
    </div>
  );
};

export default Partner;

// function Partner() {
//   return (
//     <div className="Partner-main partner mt-4">
//       <h1> Our Partners
//       </h1>
//       <img src={AWS} alt="" data-aos="zoom-in-left" className="aws-image" />
//       <img
//         src={Ethereum}
//         alt=""
//         data-aos="zoom-in-up"
//         className="ethereum-image"
//       />
//       <img
//         src={Google}
//         alt=""
//         data-aos="zoom-in-right"
//         className="google-image"
//       />
//       <img
//         src={ENS}
//         alt=""
//         data-aos="zoom-in-down"
//         className="startupindia-image"
//       />
//       <hr className="hr" />
//     </div>
//   );
// }

// export default Partner;

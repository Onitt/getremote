import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Hero() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [btnState, setbtnState] = useState(false);

  const submitForm = async (e) => {
    e.preventDefault();
    if (name === "" || email === "" || mobile === "") {
      toast.error("All fields are required");
      return;
    }
    if (name && email && mobile) {
      setbtnState(true);
      const values = {
        email: email,
        name: name,
        mobile: mobile,
      };
      console.log(values);

      let formData = new FormData(); //formdata object

      formData.append("name", name); //append the values with key, value pair
      formData.append("email", email);
      formData.append("mobile", mobile);

      fetch("https://mapi.getremote.tech/", {
        method: "POST", // or 'PUT'
        mode: "no-cors",
        headers: {
          // "Content-Type": "multipart/form-data",
          "Access-Control-Allow-Origin": "*",
        },
        body: formData,
      })
        .then((res) => {
          resetForm();
          toast.success("Data sent successfully..");
          setbtnState(false);
        })
        .catch((err) => {
          setbtnState(false);
        });
    }
  };

  const resetForm = () => {
    setEmail("");
    setName("");
    setMobile("");
  };

  return (
    <div className="home-main-div" id="home-section">
      <ToastContainer position="top-right" />
      <div className="row home-div">
        <div className="col-12 col-md-7 order-1 home-left-div">
          <h1 className="home-main-h1">
            Building Your <span className="remote-team-txt">Remote Technology Team ? </span><br /> You are at the right place.
          </h1>
          <p className="home-main-p">
            Talent is global. So should technology team be. We help you with
            onboarding top- talent across India and building a high-skilled,
            scalable & global technology team. Start working remote within 24 hours! 
          </p>
        </div>
        <div className="col-12 col-md-5 order-2">
          <ToastContainer position="top-right" />
          <div className="contact-form">
            <h2>Contact Us</h2>
            <form action="">
              <div class="my-3">
                <input
                  type="text"
                  class="form-control"
                  id="name"
                  placeholder="Name"
                  value={name}
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                />
              </div>
              <div class="mb-3">
                <input
                  type="email"
                  class="form-control"
                  id="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value);
                    console.log("email", e.target.value);
                    if (
                      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
                        e.target.value
                      ) === false
                    ) {
                      document
                        .getElementById("email")
                        .classList.add("is-invalid");
                      document.getElementById("submit-btn").disabled = true;
                    } else {
                      document
                        .getElementById("email")
                        .classList.remove("is-invalid");
                      document.getElementById("submit-btn").disabled = false;
                    }
                  }}
                />
              </div>
              <div class="mb-3">
                <input
                  type="number"
                  class="form-control"
                  id="number"
                  placeholder="Phone number"
                  value={mobile}
                  onChange={(e) => {
                    setMobile(e.target.value);
                    if (e.target.value.length < 10) {
                      document
                        .getElementById("number")
                        .classList.add("is-invalid");
                      document.getElementById("submit-btn").disabled = true;
                      // console.log
                    } else {
                      document
                        .getElementById("number")
                        .classList.remove("is-invalid");
                      document.getElementById("submit-btn").disabled = false;
                    }
                  }}
                />
              </div>
              <p className="text-center terms-text">
                By submitting, you agree to our Terms of <br /> Service and
                Privacy Policy
              </p>
              <button
                type="submit"
                class="btn hero-main-btn"
                id="submit-btn"
                onClick={submitForm}
                disabled={btnState}
              >
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
      <h2 className="promo-text">
        staff augmentation | offshore teams | fixed price projects
      </h2>
    </div>
  );
}

export default Hero;

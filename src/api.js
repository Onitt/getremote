import Axios from "axios";
const mainUrl = process.env.REACT_APP_PHP_APIS_URL;

const sendMail = async (values) => {
  try {
    const { data = {} } = await Axios.post(`${mainUrl}/send_mail.php`, values, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
    });

    return data;
  } catch (error) {
    return error;
  }
};

export default sendMail;

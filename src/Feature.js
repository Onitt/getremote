import React from "react";
import Community from "./images/community.jpeg";
import foreignFriendly from "./images/foreignFriendly.jpeg";
import virtualAssistant from "./images/VirtualAssistant.png";
import freelancing from "./images/Freelancing1.png";

function Feature() {
  const cardData = [
    {
      logo: Community,
      title: "Dedicated Resource Model",
      content:
        "In this model, engage our best resources on monthly billing model. The resource works 160 hours a month as a part of your team, we bill you monthly. Works best for smaller teams (less than 10 members). Onboarding time is 24 hours.",
      offer:"Save up-to 50% on your current team costs.",
    },
    {
      logo: foreignFriendly,
      title: "Offshore Development Centre (ODC) Model",
      content:
        "In this model, we help you hire the required talent from the market. We shortlist the candidates that fit your budget, you interview them, we onboard them on our payroll. The team works as an extended arm of your organisation, we bill monthly. Works best for large teams (5-50 members). Onboarding time is 4 weeks.",
        offer:"Save up-to 70% on your current team costs.",
    },
    {
      logo: virtualAssistant,
      title: "Project Based Teams",
      content:
        "Hire the whole or partial team for the duration of the project. Share the roles required, we build the team for you, the team works as a part of your organisation for the duration of the project. Onboarding time is 3-5 days, on monthly billing schedule.",
        offer:"Save up-to 50% on your total project cost.",
    },
    {
      logo: freelancing,
      title: "Fixed Price Projects",
      content:
        "We take the project requirements from you and provide the cost and timeline estimates. The project is billed as per milestones decided at the start of the engagement. Kick-off time, 2-3 days.",
        offer:"Save up-to 50% on your total project cost.",
    },
  ];

  return (
    <div className="container feature-container">
      <div className="row mt-5 mb-5">
        {cardData.map((val, index) => {
          return (
            <div className="col-12 col-md-6">
              <div class="card mt-4 feature-card">
                <div class="card-body">
                  <img src={val.logo} alt="" className="feature-card-img" />
                  <h5 class="card-title mt-4 ">{val.title}</h5>
                  <p class="card-text text-black">{val.content}</p>
                  <b>{val.offer}</b>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Feature;

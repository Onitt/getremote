import "./App.css";
import Navbar from "./Navbar";
import Hero from "./Hero";
import Feature from "./Feature";
import About from "./About";
import Counter from "./Counter";
import Contact from "./Contact";
import Partner from "./Partner";
import Footer from "./Footer";
import "./css/style.css";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero />
      <Feature />
      <About />
      <Counter />
      <Contact />
      <Partner />
      <Footer />
    </div>
  );
}

export default App;

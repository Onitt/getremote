import React, { useState } from "react";
import contactImg1 from "./images/talk2us.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Link} from 'react-router-dom';

function Contact() {
  const [email, setEmail] = useState("");
  const [btnstate, setbtnState] = useState(false);

  const submitForm = async (e) => {
    e.preventDefault();
    if (!email) {
      toast.error("Email Required");
      return;
    }
    if (email) {
      const values = {
        email: email,
      };
      let formData = new FormData(); //formdata object

      formData.append("email", email); //append the values with key, value pair

      fetch("https://mapi.getremote.tech/", {
        method: "POST", // or 'PUT'
        mode: "no-cors",
        headers: {
          // "Content-Type": "multipart/form-data",
          "Access-Control-Allow-Origin": "*",
        },
        body: formData,
      })
        .then((res) => {
          setEmail("");
          toast.success("Data sent successfully..");
          setbtnState(false);
        })
        .catch((err) => {
          setbtnState(false);
        });
    }
  };

  return (
    <div>
      <div className="container-fluid row contact-div1">
        <div className="col-12 col-md-6">
          <img src={contactImg1} alt="" />
        </div>
        <div className="col-12 col-md-6 ps-3">
          <h3 className="mt-4">Talk to us. Let’s start.</h3>
          <p className="mt-4">
            The best way to start our relationship will be a short call with our
            technology leads. We would love to understand your requirements in
            first person and propose you a solution that is a best fit for you
            keeping in mind your timelines and budget.
          </p>
          <p>
            Our top leadership is deeply involved with our clients to ensure we
            deliver you the best. Share your contact details or write to us at &nbsp;
             <a href="mailto:info@remote.tech">info@getremote.tech</a> and we will set up a call with you to take
            things forward.
          </p>
          <a href="#contact-section">
            <button className="btn contact-div1-btn">Contact Us</button>
          </a>
        </div>
      </div>
      <div className="container-fluid row contact-div2 mb-5">
        <div className="col-12 col-md-5">
          <h2>
            Share your contact details for setting up a call with our experts.
          </h2>
        </div>
        <div className="col-12 col-md-7 email-input">
          <form action="" className="d-flex mt-4">
            <input
              type="email"
              class="form-control"
              id="email2"
              placeholder="Email"
              style={{ width: "70%" }}
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
                console.log("email", e.target.value);
                if (
                  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
                    e.target.value
                  ) === false
                ) {
                  document.getElementById("email2").classList.add("is-invalid");
                  document.getElementById("submit-btn2").disabled = true;
                } else {
                  document
                    .getElementById("email2")
                    .classList.remove("is-invalid");
                  document.getElementById("submit-btn2").disabled = false;
                }
              }}
            />
            <button
              className="btn contact-div2-btn"
              id="submit-btn2"
              disabled={btnstate}
              onClick={submitForm}
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Contact;

import React from "react";

function Footer() {
  return (
    <div className="mt-5" id="contact-section">
        <h2>Get In Touch</h2>
        <p>We will get back to you as soon as possible</p>
        <hr style={{ width: "30%" }} className="ms-auto me-auto" />
        <div className="row footer-row">
          {/* <div className="col-12 col-md-3">
            <h3>Postal Address:</h3>
            <p>
              PO Box 16122 Collins Street West <br /> Victoria 8007 Australia
            </p>
          </div>
          <div className="col-12 col-md-3">
            <h3>HeadQuarters</h3>
            <p>
              121 King Street, Melbourne <br /> Victoria 3000 Australia
            </p>
          </div> */}
          <div className="col-12 col-md-6 phone-leftside">
            <h3>Phone</h3>
            <p>
              Phone Number: +91-9650806794
              <br />
              Phone Number: +91-9560332121
            </p>
          </div>
          <div className="col-12 col-md-6 email-rightside">
            <h3>E-mail </h3>
            <p>
              info@getremote.tech <br /> prerna@getremote.tech
            </p>
          </div>
      </div>
      <div className="footer-div-main">
        <div className="row footer-div">
          <div className="col-md-1"></div>
          <div className="col-12 col-md-6 text-white mt-4">
            <h2>The best place to take your team global.</h2>
          </div>
          <div className="col-12 col-md-4 lets-startedBtn">
            <a href="#home-section">
              <button className="btn footer-start-btn">
                Let's Get Started
              </button>
            </a>
            <a href="#home-section">
              <button className="btn upArrowbtn">^</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;

import React from "react";
import Logo from "./images/logo.png";

function Navbar() {
  return (
    <div className="navbar-main">
      <nav class="navbar navbar-expand-lg navbar-light bg-light ">
        <div class="container">
          <div>
            <a class="navbar-brand p-0" href="#home-section">
              <img src={Logo} alt="" className="nav-logo" />
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav mb-2 mb-lg-0 navbar-ul ms-auto">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="#home-section">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#about-section">
                  About
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact-section">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;

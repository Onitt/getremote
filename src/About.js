import React from "react";
import AboutImage from "./images/aboutImage.jpg";
import corouselImage1 from "./images/Chg1.png";
import corouselImage2 from "./images/Chg2.png";
import corouselImage3 from "./images/Chg3.png";

function About() {
  return (
    <section id="about-section">

      <div className="">
        <div className="row about-div2 mt-5">
          <div className="col-12 col-md-6">
            <div
              id="carouselExampleSlidesOnly"
              class="carousel slide"
              data-bs-ride="carousel"
            >
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src={corouselImage1} class="d-block w-100" alt="..." />
                </div>
                <div class="carousel-item">
                  <img src={corouselImage2} class="d-block w-100" alt="..." />
                </div>
                <div class="carousel-item">
                  <img src={corouselImage3} class="d-block w-100" alt="..." />
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 about-div2-text mt-2">
            <h2>Built on decades of experience and trust.</h2>
            <p className="mt-3">
              The founders of Get Remote have worked for world’s top technology
              firms and have extensive experience of delivering complex
              technology projects across the globe for Fortune 100 and Fortune
              500 firms such as Merck, World Bank, UNHCR among others.
            </p>
            <p>
              Our team has been trained to work seamlessly as a part of global
              remote teams and have delivered amazing results for our customers.
            </p>
            <button className=" btn about-div2-btn">View Our Works</button>
          </div>
        </div>
      </div>


      <div className="container about-div">
        <h2>How it works?</h2>
        <h5>
          You can engage with us in two modes. Dedicated Resources & Project
          Based.
        </h5>
        <hr style={{ width: "30%" }} className="ms-auto me-auto mb-5" />
        <div className="row">
          <div className="col-12 col-md-6 about-left-section">
            <h4>What do we do?</h4>
            <p className="mt-3">
              We are a new-age technology firm that understands your needs for technology talent. We understand your need to have a global technology team. With 30+ years of experience between the founders, we help you onboard the candidates with the right skills, in the shortest possible lead time within your budget to your technology team.
            </p>
            <p>In the past, the founders have worked with clients across the globe. We have worked with single member teams to giants like World Bank, US Foods and Honda America.</p>
            <div className="row p-0 mt-4">
              <div className="col-12 col-md-6 order-1">
                <button
                  className="about-btn about-btn-click"
                  id="about-btn1"
                  onClick={() => {
                    document
                      .getElementById("about-btn1")
                      .classList.add("about-btn-click");
                    document
                      .getElementById("about-btn2")
                      .classList.remove("about-btn-click");

                    document.getElementById("about-p-text").innerHTML =
                      "The resource works full time for you and is billed on a monthly schedule. You can onboard candidates either via the Dedicated Resource Model or Offshore Development Centre model";
                    document.getElementById("about-p-text2").innerHTML =
                      "This model is best-fit for the needs of creating a long term team in India. Clients have saved up-to 70% on their current team costs using our ODC and DR staffing models.";
                  }}
                >
                  Dedicated Resources
                </button>
              </div>
              <div className="col-12 col-md-6 order-2">
                <button
                  id="about-btn2"
                  className="about-btn"
                  onClick={() => {
                    document
                      .getElementById("about-btn2")
                      .classList.add("about-btn-click");
                    document
                      .getElementById("about-btn1")
                      .classList.remove("about-btn-click");
                    document.getElementById("about-p-text").innerHTML =
                      "Instead of hiring a dedicated team, share your project requirements with us. We manage the project team and deliver the project within your budget as per the agreed timelines.";
                    document.getElementById("about-p-text2").innerHTML =
                      "The biggest advantage in this model is that you do not have to manage the team at all. You just engage with the project SPOC and pay as per the milestones agreed.<br></br>";
                  }}
                >
                  Fixed Price Projects
                </button>
              </div>
            </div>

            <p id="about-p-text" className="mt-4">
              The resource works full time for you and is billed on a monthly
              schedule. You can onboard candidates either via the Dedicated
              Resource Model or Offshore Development Centre model
            </p>
            <p id="about-p-text2">
            This model is best-fit for the needs of creating a long term team in India. Clients have saved up-to 70% on their current team costs using our ODC and DR staffing models.
            </p>
          </div>
          <div className="col-12 col-md-6">
            <img src={AboutImage} className="aboutImage" alt="" />
          </div>
        </div>
      </div>

    </section>
  );
}

export default About;
